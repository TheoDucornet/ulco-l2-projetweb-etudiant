<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

//Début de la session
session_start();

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store', 'controller\StoreController@store');

//GET "/store/{:num}"
$router->get('/store/{:num}','controller\StoreController@product');

//GET "/account"
$router->get('/account','controller\AccountController@account');

//POST "/login"
$router->post('/account/login','controller\AccountController@login');

//POST "/signin"
$router->post('/account/signin','controller\AccountController@signin');

//GET "/login"
$router->get('/account/logout','controller\AccountController@logout');

//POST "/postComment/{:num}
$router->post('/postComment/{:num}','controller\CommentController@postComment');

//POST "/search"
$router->post('/search','controller\StoreController@search');

//GET "/account/infos"
$router->get('/account/infos','controller\AccountController@infos');

//POST "/account/update"
$router->post('/account/update','controller\AccountController@update');

//GET "/cart"
$router->get('/cart','controller\CartController@displayCart');

//POST "/cart/add"
$router->post('/cart/add','controller\CartController@addCart');

//POST "/cart/update"
$router->post('/cart/update','controller\CartController@update');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
