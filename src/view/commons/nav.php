<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if(isset($_SESSION['mail'])&& isset($_SESSION['id']) && isset($_SESSION['firstname']) && isset($_SESSION['lastname'])){?>
        <a class="account" href="/account/infos">
            <img src="/public/images/avatar.png">
            <?php echo($_SESSION['firstname']." ".$_SESSION['lastname']); ?>
        </a>
        <a href="/cart">
            PANIER
        </a>
        <a href="/account/logout">
            DECONNEXION
        </a>
    <?php }else {?>
    <a class="account" href="/account">
        <img src="/public/images/avatar.png">
        Compte
    </a>
    <?php }?>
</nav>