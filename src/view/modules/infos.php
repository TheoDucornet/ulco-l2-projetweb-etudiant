<?php
if(isset($_GET['status'])){
    if($_GET['status']=="accountUpdateSuccess"){
        ?>
        <div class="box info"> Vos informations ont été correctement actualisées</div>
        <?php
    }
    else if($_GET['status']=="accountUpdateFailed"){
        ?>
        <div class="box error">Vos informations n'ont pas pu être actualisées</div>
        <?php
    }
}
?>

<div id="account">
    <div>
        <h2>Informations du compte</h2>
                <br>
        <h3> Informations personnelles</h3>
        <form method="post" action="/account/update">
            <div style="display: flex; padding: 3px">
                Prénom
                <input type="text" name="firstname" value="<?=$_SESSION['firstname'] ?>" style="margin-left: 84px">
            </div>
            <div style="display: flex; padding: 3px">
                Nom
                <input type="text" name="lastname" value="<?=$_SESSION['lastname'] ?>" style="margin-left: 105px">
            </div>
            <div style="display: flex; padding: 3px">
                Adresse mail
                <input type="text" name="mail" value="<?=$_SESSION['mail']?>" style="margin-left: 50px; width: 400px">
            </div>
            <input type="submit" value="Modifier mes informations">
        </form>
        <br>
        <h3>
            Commandes
        </h3>
        <div>
            Tu n'as pas de commandes en cours
        </div>
    </div>
</div>