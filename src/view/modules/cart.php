<div id="cart" style="margin: 20px;">
    <h2>Panier</h2>
    <br>
    <?php
    if(empty($_SESSION['cart'])){
        ?>
        <p>Ton panier est vide</p>
        <?php
    } else{
        //Partie génération panier
        ?>
        <form method="post" action="/cart/update">
        <?php
        //Sert pour l'id de $_POST quand on met le produit à jour
        $i=0;
        foreach ($params['cart'] as $c){
            ?>
            <div class="content" style="display: flex;padding:10px;">
                <p style="width: 800px; display: flex; flex-direction: column; justify-content: center">
                    <img src="/public/images/<?=$c['image']?>" style="max-width: 100%;max-height: 100%;">
                </p>
                <div style="width: 5px"></div>
                <div style="padding: 5px;width: 800px">
                    <p style="background-color: var(--bg-accent);display: inline-block;font-size: 18px;font-weight: 600;margin-bottom: 12px;padding: 8px 12px;">
                        <?=$c['categorie']?></p>
                    <h2><?= $c['name']?></h2>
                </div>
                <div style="width: 100%"></div>
                <div>
                    <p>Quantité:</p>
                    <div style="display: flex; padding: 10px">
                        <button type="button" id="boutonMoins"  style="background-color: #fff;border: 2px solid var(--color-main);color: var(--color-main);font-family: var(--font-main);font-size: 20px;padding: 2px 8px;">
                            -
                        </button>
                        <div style="width: 5px;height: auto;display: inline-block"></div>
                        <button type="button" id="quantite" style="background-color: #fff;border: 2px solid var(--color-main);color: var(--color-main);font-family: var(--font-main);font-size: 20px;padding: 2px 8px;">
                            <?=$c['quantite']?>
                        </button>
                        <input type="hidden" id="quantiteHidden" name="<?=$i?>" value="<?=$c['quantite']?>">
                        <?php $i++ ?>
                        <input type="hidden" id="idHidden" name="<?=$i?>" value="<?=$c['id']?>">
                        <?php $i++ ?>
                        <div style="width: 5px;height: auto;display: inline-block"></div>
                        <button type="button" id="boutonPlus" style="background-color: #fff;border: 2px solid var(--color-main);color: var(--color-main);font-family: var(--font-main);font-size: 20px;padding: 2px 8px;">
                            +
                        </button>
                    </div>
                </div>
                <div style="width: 100%"></div>
                <div style="margin-right: 50px">
                    Prix Unitaire:
                    <h3 id="prix">
                        <?=$c['prix']?>€
                    </h3>
                </div>
            </div>
            <div style="border-top: 1px solid rgba(0, 0, 0, .2);"></div>
            <?php
        }
        //Partie calcul du total
        $total=0;
        foreach ($params['cart'] as $c){
            $total+=$c['prix']*$c['quantite'];
        }
    ?>
        <div style="display: flex; margin-top: 10px">
            <div style="width: 100%"></div>
            <div>
                <p>Prix total du panier:</p>
                <h3 id="total"><?= $total ?> €</h3>
            </div>
        </div>
        <?php
    }
    ?>
    <div style="display: flex; padding: 15px">
        <button style="background-color: #fff;border: 2px solid var(--color-main);color: var(--color-main);font-family: var(--font-main);font-size: 20px;padding: 2px 8px;">
            Procéder au paiement
        </button>
        <div style="width: 100%"></div>
        <button type="submit" style="background-color: #fff;border: 2px solid var(--color-main);color: var(--color-main);font-family: var(--font-main);font-size: 20px;padding: 2px 8px;">
            Mettre à jour le panier
        </button>
    </div>
        </form>
</div>

<script src="../../../public/scripts/cart.js"></script>
