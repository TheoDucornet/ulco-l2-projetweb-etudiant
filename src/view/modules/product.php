<?php

$product= $params["product"][0];
$comments=$params["comments"];

echo '<script>';
echo 'console.log('. json_encode($product) .')';
echo '</script>';

?>

<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $product["image"]?>">
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $product["image"]?>">
                </div>
                <div>
                        <img src="/public/images/<?= $product["image_alt1"]?>">
                </div>
                <div>
                    <img src="/public/images/<?= $product["image_alt2"]?>">
                </div>
                <div>
                    <img src="/public/images/<?= $product["image_alt3"]?>">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?= $product["name"]?></p>
            <h1><?= $product[0]?></h1>
            <p class="product-price"><?= $product["price"]?>€</p>
            <form id="quantite" method="post" action="/cart/add">
                <button type="button">-</button>
                <button type="button" name="nombre" value=1>1</button>
                <button type="button">+</button>
                <input type="hidden" name="nomproduit" value="<?=$product[0]?>">
                <input type="hidden" name="id" value="<?=$product['id']?>">
                <input type="hidden" name="prix" value="<?=$product[1]?>">
                <input type="hidden" name="image" value="<?=$product["image"]?>">
                <input type="hidden" name="categorie" value="<?=$product["name"]?>">
                <input type="hidden" name="quantite" id="quantiteHidden" value="1">
                <input type="submit" value="Ajouter au panier">
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?= $product["spec"] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if(isset($comments)){
                foreach($comments as $c){?>
                    <div class="product-comment">
                        <p class="product-comment-author"><?= $c["firstname"]?> <?= $c["lastname"]?></p>
                        <p><?= $c["content"]?></p>
                    </div>
                <?php }
            }else{
                ?>
                <p>Il n'y a pas d'avis pour ce produit</p>
            <?php } ?>
            <?php if(isset($_SESSION['mail'])&& isset($_SESSION['id']) && isset($_SESSION['firstname']) && isset($_SESSION['lastname'])){?>
                <form class="product-comment" method="post" action="/postComment/<?= $product["id"]?>">
                    <input type="text" name="comment" placeholder="Rédiger un commentaire">
                    <input type="submit" value="Poster votre commentaire">
                </form>
            <?php }?>
        </div>
    </div>
</div>
<script src="../../../public/scripts/product.js"></script>
