<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(){
      $db = \model\Model::connect();

      $sql = "SELECT product.id, product.name, price, image,category.name FROM product INNER JOIN category WHERE product.category=category.id";


      $req = $db->prepare($sql);
      $req->execute();

      return $req->fetchAll();
  }

    static function infoProduct(int $id){
        $db = \model\Model::connect();

        $sql = "SELECT product.name,product.price,image,image_alt1,image_alt2,image_alt3,spec,category.name,product.id FROM product 
                    INNER JOIN category WHERE product.category=category.id 
                        AND  product.id=$id";

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }

    static function searchProducts($search,$category,$order){
        $db = \model\Model::connect();
        //Selection de tous les produits:
        $sql = "SELECT product.id, product.name, price, image,category.name FROM product INNER JOIN category WHERE product.category=category.id";
        //On ajoute les filtres:
        if(isset($search))
            $sql.=" AND product.name LIKE '%$search%'";
        if(isset($category)){
            $sql .=" AND (";
            foreach ($category as $c){
                if($c==$category[0])
                    $sql.=" category.name LIKE '".$c."'";
                else
                    $sql.=" OR category.name LIKE '".$c."'";
            }
            $sql.=")";
        }
        if(isset($order)) {
            if ($order == "croissant")
                $sql .= " ORDER BY product.price ASC";
            if ($order == "decroissant")
                $sql .= " ORDER BY product.price DESC";
        }

        echo '<script>';
        echo 'console.log('. json_encode($sql) .')';
        echo '</script>';

        $req = $db->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

}