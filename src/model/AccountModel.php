<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password): bool{
        //Verification taille firstname / lastname
        if(strlen($firstname) < 2 || strlen($lastname) < 2)
            return false;
        //Verification mail valide
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL))
            return false;
        //Verification mail existe pas déjà
        $db = \model\Model::connect();
        $sql = "SELECT * FROM account WHERE mail='$mail'";
        $req = $db->prepare($sql);
        $req->execute();
        $answer=$req->fetchAll();
        /*
        echo '<script>';
        echo 'console.log('. json_encode( $answer) .')';
        echo '</script>'; */
        if($answer != null)
            return false;
        if(strlen($password) < 6)
            return false;
        return true;
    }

    static function signin($firstname,$lastname,$mail,$password): bool{
        if(self::check($firstname,$lastname,$mail,$password)){
            $db = \model\Model::connect();
            $sql = "INSERT INTO account(firstname,lastname,mail,password) VALUES ('".htmlspecialchars($firstname)."','".htmlspecialchars($lastname)."'
            ,'".htmlspecialchars($mail)."','".htmlspecialchars($password)."')";
            $req = $db->prepare($sql);
            $req->execute();
            $req->fetchAll();
            return true;
        }
        return false;
    }

    static  function login($mail, $password){
        $db = \model\Model::connect();
        $sql ="SELECT * FROM account WHERE mail='$mail' AND password='$password'";
        $req = $db->prepare($sql);
        $req->execute();
        return $req->fetchAll();

    }

    static function checkUpdate($id,$firstname, $lastname, $mail){
        //Verification taille firstname / lastname

        if(strlen($firstname) < 2 || strlen($lastname) < 2)
            return false;
        //Verification mail valide
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL))
            return false;
        //Verification mail existe pas déjà chez un autre utilisateur
        $db = \model\Model::connect();
        $sql = "SELECT * FROM account WHERE mail='$mail' AND account.id != '$id'";
        $req = $db->prepare($sql);
        $req->execute();
        $answer=$req->fetchAll();
        /*
        echo '<script>';
        echo 'console.log('. json_encode( $answer) .')';
        echo '</script>';
        */
        if($answer != null)
            return false;
        return true;
    }

    static function update($id,$firstname,$lastname,$mail): bool{
        if(self::checkUpdate($id,$firstname,$lastname,$mail)){
            $db=\model\Model::connect();
            $sql = "UPDATE account SET account.firstname='".htmlspecialchars($firstname)."', account.lastname='".htmlspecialchars($lastname)."'
                , account.mail='".htmlspecialchars($mail)."' WHERE account.id=".$id;
            $req = $db->prepare($sql);
            $req->execute();
            $req->fetchAll();
            return true;
        }
        return false;
    }



}