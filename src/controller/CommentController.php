<?php


namespace controller;


class CommentController
{
    public function postComment(int $id)
    {

        \model\CommentModel::insertComment($id, $_POST['comment'], $_SESSION['id']);
        $product = \model\StoreModel::infoProduct($id);
        /*
        echo '<script>';
        echo 'console.log('. json_encode( $product) .')';
        echo '</script>';
        */
        if (empty($product)) {
            header("Location: /store");
            exit();
        } else {
            $product= \model\StoreModel::infoProduct($id);
            $comments=\model\CommentModel::listComment($id);
            $params=array(
                "title"=>"Product ".$id,
                "module"=>"product.php",
                "product"=>$product,
                "comments"=>$comments
            );
            \view\Template::render($params);
        }
    }

}