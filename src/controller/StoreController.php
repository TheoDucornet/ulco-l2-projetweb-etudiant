<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProducts();

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "products" => $products
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void{
      $product= \model\StoreModel::infoProduct($id);
      $comments=\model\CommentModel::listComment($id);
/*
      echo '<script>';
      echo 'console.log('. json_encode( $comments) .')';
      echo '</script>';
*/
      if(empty($product)){
          header("Location: /store");
          exit();
      }
      else{
          $params=array(
              "title"=>"Product ".$id,
              "module"=>"product.php",
              "product"=>$product,
              "comments"=>$comments
          );
          \view\Template::render($params);
      }
  }

  public function search():void{
      $search=null;
      $category=null;
      $order=null;
      if(isset($_POST["search"]))
          $search=$_POST["search"];
      if(isset($_POST["category"]))
          $category=$_POST["category"];
      if(isset($_POST["order"]))
          $order=$_POST["order"];

      $categories = \model\StoreModel::listCategories();
      $products=\model\StoreModel::searchProducts($search,$category,$order);
      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );
      \view\Template::render($params);
  }
}