<?php


namespace controller;


class AccountController
{
    public function account(): void{
        $params=array(
            "title"=>"Account",
            "module"=>"account.php",
        );
        \view\Template::render($params);
    }

    public function signin(): void{
        echo '<script>';
        echo 'console.log('. json_encode( $_POST) .')';
        echo '</script>';

        $test=\model\AccountModel::signin($_POST['userfirstname'],$_POST['userlastname'],$_POST['usermail'],$_POST['userpass']);

        if($test)
            header("Location: /account?status=account_created");
        else
            header("Location: /account?status=account_failed");
        exit();
    }

    public function login():void {
        $user=\model\AccountModel::login($_POST['usermail'],$_POST['userpass']);
        if($user !=null){
            /*
            echo '<script>';
            echo 'console.log('. json_encode( $user[0]) .')';
            echo '</script>'; */
            $_SESSION['id']=$user[0]['id'];
            $_SESSION['firstname']=$user[0]['firstname'];
            $_SESSION['lastname']=$user[0]['lastname'];
            $_SESSION['mail']=$user[0]['mail'];
            header("Location: /store");
            exit();
        }
        header("Location: /account?status=login_fail");
    }

    public function logout(): void{
        session_destroy();
        header("Location: /account?status=disconnect");
        exit();
    }

    public function infos():void{
        if(isset($_SESSION['mail']) && isset($_SESSION['id']) && isset($_SESSION['firstname']) && isset($_SESSION['lastname'])){
            $params = [
                "title"  => "Infos",
                "module" => "infos.php"
            ];
            \view\Template::render($params);
        }
        else
            header("Location: /error");

    }

    //Pour modifier les informations du compte en faisant appel au AccountModel
    public function update():void{
        if(isset($_SESSION['mail']) && isset($_SESSION['id']) && isset($_SESSION['firstname']) && isset($_SESSION['lastname'])){
            //Envoi à la BDD avec check des infos
            if(\model\AccountModel::update($_SESSION['id'],$_POST['firstname'],$_POST['lastname'],$_POST['mail'])){
                //Si la BDD a changée, les informations de session aussi
                $_SESSION['firstname']=$_POST['firstname'];
                $_SESSION['lastname']=$_POST['lastname'];
                $_SESSION['mail']=$_POST['mail'];
                header("Location: /account/infos?status=accountUpdateSuccess");
            }
            else{
                header("Location: /account/infos?status=accountUpdateFailed");

            }
        }
        else
            header("Location: /error");
    }

}