<?php
namespace controller;


class CartController
{
    public function displayCart(): void{
        $params=array(
            "title"=>"Cart",
            "module"=>"cart.php",
            "cart"=>$_SESSION['cart']
        );
        \view\Template::render($params);
    }

    public function addCart(): void{

        echo '<script>';
        echo 'console.log('. json_encode( $_POST) .')';
        echo '</script>';

        //On met les informations nécessaires dans la session:

        if(!isset($_SESSION['cart']))
            $_SESSION['cart']=array();

        if(!empty($_SESSION['cart'][$_POST['id']])){
            $_SESSION['cart'][$_POST['id']]['quantite']+=$_POST['quantite'];
        }
        else
            $_SESSION['cart'][$_POST['id']]=array(
                'name' => $_POST['nomproduit'],
                'categorie' => $_POST['categorie'],
                'image' => $_POST['image'],
                'prix'=> $_POST['prix'],
                'quantite'=>$_POST['quantite'],
                'id'=>$_POST['id']);

        echo '<script>';
        echo 'console.log('. json_encode( $_SESSION['cart']) .')';
        echo '</script>';

        //Et on repart dans le panier
        header("Location: /cart");
        exit();
    }

    //Fonction en plus pour mettre à jour le panier
    public function update(): void{
        echo '<script>';
        echo 'console.log('. json_encode($_POST).')';
        echo '</script>';
        //Comme on ne peut pas directement savoir la lenght de $_POST:
        $i=0;
        foreach ($_POST as $c){
            $i++;
        }
        //Faut récupérer l'id du produit pr mettre à jour ds session
        for($j=0;$j<$i;$j=$j+2){
            $_SESSION['cart'][$_POST[$j+1]]['quantite'] = $_POST[$j];
            /*
            echo '<script>';
            echo 'console.log('. json_encode($_POST[$j]).')';
            echo '</script>';
            echo '<script>';
            echo 'console.log('. json_encode($_POST[$j+1]).')';
            echo '</script>';
            */
        }
        echo '<script>';
        echo 'console.log('. json_encode($_SESSION['cart']).')';
        echo '</script>';
        //Et on repart dans le panier
        header("Location: /cart");
        exit();


    }
}