let boutonmoins=document.querySelectorAll("#boutonMoins")
let boutonplus=document.querySelectorAll("#boutonPlus")
let quantite=document.querySelectorAll("#quantite")
console.log(boutonmoins)
console.log(boutonplus)
console.log(quantite)
let total=document.getElementById("total")
let prix=document.querySelectorAll("#prix")
let quantiteHidden=document.querySelectorAll("#quantiteHidden")
console.log(prix)
console.log(quantiteHidden)

for (let i = 0; i < quantite.length; i++) {

    //Association au bouton moins
    boutonmoins[i].addEventListener('click',function (){
        //On diminue la quantité si elle vaut plus de 1
        if(parseInt(quantite[i].innerHTML,10)>1){
            quantite[i].innerHTML=parseInt(quantite[i].innerHTML,10)-1
            quantiteHidden[i].value=parseInt(quantiteHidden[i].value,10)-1
            //Recalcul du total
            total.innerHTML=(parseInt(total.innerHTML,10)-parseInt(prix[i].innerHTML)).toString()+" €"
        }
    })

    //Association au bouton plus
    boutonplus[i].addEventListener('click',function(){
        //On augmente la quantité si elle vaut moins que 5
        if(parseInt(quantite[i].innerHTML,10)<5){
            quantite[i].innerHTML=parseInt(quantite[i].innerHTML,10)+1
            quantiteHidden[i].value=parseInt(quantiteHidden[i].value,10)+1
            //Recalcul du total
            total.innerHTML=(parseInt(total.innerHTML,10)+parseInt(prix[i].innerHTML)).toString()+" €"
        }
    })
}
