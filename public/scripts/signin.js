//Verif lastname
let userlastname=document.getElementsByName("userlastname")
userlastname[0].onchange = (event =>{
    event.target.classList.remove("valid")
    event.target.classList.remove("invalid")
    $value= event.target.value;
    if($value.length >= 2)
        event.target.classList.add("valid")
    else
        event.target.classList.add("invalid")
})

//Verif firstname
let userfirstname=document.getElementsByName("userfirstname")
userfirstname[0].onchange=(event=>{
    event.target.classList.remove("valid")
    event.target.classList.remove("invalid")
    $value=event.target.value;
    if($value.length >= 2)
        event.target.classList.add("valid")
    else
        event.target.classList.add("invalid")
})

//Verif mail
let usermail=document.getElementsByName("usermail")
for(let i=0;i<2;i++){
    usermail[i].onchange=(event=>{
        event.target.classList.remove("valid")
        event.target.classList.remove("invalid")
        $value=event.target.value;
        const regex= new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}")
        if(regex.test($value))
            event.target.classList.add("valid")
        else
            event.target.classList.add("invalid")
    })
}


//Verif mot de passe
let userpass=document.getElementsByName("userpass")
//Création mot de passe
userpass[1].onchange=(event =>{
    event.target.classList.remove("valid")
    event.target.classList.remove("invalid")
    $value=event.target.value;
    const regex= new RegExp("[a-z0-9._%+-]{6,}");
    const regexChiffre=new RegExp("[0-9]");
    const regexLettre=new RegExp("[a-z]");
    if(regex.test($value) && regexChiffre.test($value) && regexLettre.test($value))
        event.target.classList.add("valid")
    else
        event.target.classList.add("invalid")
    userpass[2].classList.remove("valid")
    userpass[2].classList.remove("invalid")
    if($value == userpass[2].value)
        userpass[2].classList.add("valid")
    else
        userpass[2].classList.add("invalid")
})
//Repetition mot de passe
userpass[2].onchange=(event=>{
    event.target.classList.remove("valid")
    event.target.classList.remove("invalid")
    $value=event.target.value;
    if($value==userpass[1].value)
        event.target.classList.add("valid")
    else
        event.target.classList.add("invalid")
})

